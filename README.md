It's All OK
===

An aggregate status reporter. This chrome plugin produces a single green-red-yellow status from a
list of CI jobs. When you click on the icon it shows you each job's status.

