let color = '#3aa757';

// TODO(jmyounker): Replace with new API.
chrome.runtime.onInstalled.addListener(() => {
    chrome.storage.sync.set({color});
    console.log('Default background color set to %cgreen', `color: ${color}`);
});
